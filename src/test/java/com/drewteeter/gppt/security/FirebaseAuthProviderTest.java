package com.drewteeter.gppt.security;

import com.drewteeter.gppt.user.model.User;
import com.drewteeter.gppt.user.UserService;
import com.google.common.collect.Sets;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class FirebaseAuthProviderTest {
    private FirebaseAuthProvider provider;
    @Mock
    private FirebaseAuth firebaseAuth;
    @Mock
    private UserService userService;

    @BeforeEach
    public void setup() {
        provider = new FirebaseAuthProvider(firebaseAuth, userService);
    }

    @Test
    @DisplayName("Does return null given authentication is not of supported type")
    void returnsNullUnsupportedAuthClass() {
        assertThat(provider.authenticate(new UsernamePasswordAuthenticationToken("test", "test"))).isNull();
        verifyZeroInteractions(userService, firebaseAuth);
    }

    @Test
    @DisplayName("Does return null given FirebaseAuthException on verify")
    void returnsNullOnFirebaseAuthException() throws FirebaseAuthException {
        when(firebaseAuth.verifyIdToken("abcd-1234")).thenThrow(new FirebaseAuthException("INVALID_TOKEN", "Unable to validate token"));
        assertThat(provider.authenticate(new RawFirebaseAuthenticationToken("abcd-1234"))).isNull();
        verifyZeroInteractions(userService);
    }

    @Test
    @DisplayName("Does create user and return corresponding token if user does not exist")
    void createUserFlow() throws FirebaseAuthException {
        User user = new User("4321-dcba", "test@drewteeter.com", "Test User",
                "https://drewteeter.com/mockpic.jpg", Sets.newHashSet(Roles.USER.name()));
        FirebaseToken firebaseToken = mock(FirebaseToken.class);
        when(firebaseToken.getUid()).thenReturn("4321-dcba");
        when(firebaseToken.getEmail()).thenReturn("test@drewteeter.com");
        when(firebaseToken.getName()).thenReturn("Test User");
        when(firebaseToken.getPicture()).thenReturn("https://drewteeter.com/mockpic.jpg");
        when(firebaseAuth.verifyIdToken("abcd-1234")).thenReturn(firebaseToken);
        when(userService.getUserById("4321-dcba")).thenReturn(Optional.empty());
        when(userService.saveUser(user)).thenReturn(user);
        Authentication token = provider.authenticate(new RawFirebaseAuthenticationToken("abcd-1234"));
        SoftAssertions tokenAssertions = new SoftAssertions();
        tokenAssertions.assertThat(token)
                .as("FirebaseAuthenticationToken").isInstanceOf(FirebaseAuthenticationToken.class)
                .as("Credentials").isSameAs(firebaseToken)
                .as("Principal").isEqualTo(user);
        verify(userService).getUserById("4321-dcba");
        verify(userService).saveUser(user);
    }

    @Test
    @DisplayName("Does return corresponding token if user does already exists")
    void existingUserFlow() throws FirebaseAuthException {
        User user = new User("4321-dcba", "test@drewteeter.com", "Test User",
                "https://drewteeter.com/mockpic.jpg", Sets.newHashSet(Roles.USER.name()));
        FirebaseToken firebaseToken = mock(FirebaseToken.class);
        when(firebaseToken.getUid()).thenReturn("4321-dcba");
        when(firebaseAuth.verifyIdToken("abcd-1234")).thenReturn(firebaseToken);
        when(userService.getUserById("4321-dcba")).thenReturn(Optional.of(user));
        Authentication token = provider.authenticate(new RawFirebaseAuthenticationToken("abcd-1234"));
        SoftAssertions tokenAssertions = new SoftAssertions();
        tokenAssertions.assertThat(token)
                .as("FirebaseAuthenticationToken").isInstanceOf(FirebaseAuthenticationToken.class)
                .as("Credentials").isSameAs(firebaseToken)
                .as("Principal").isEqualTo(user);
        verify(userService).getUserById("4321-dcba");
        verify(userService, never()).saveUser(user);
    }
}