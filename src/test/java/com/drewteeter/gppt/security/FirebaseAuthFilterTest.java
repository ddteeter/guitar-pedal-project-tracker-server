package com.drewteeter.gppt.security;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FirebaseAuthFilterTest {
    private FirebaseAuthFilter filter;
    @Mock
    private FilterChain filterChain;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @BeforeEach
    void setup() {
        filter = new FirebaseAuthFilter();
    }

    @BeforeEach
    void clearContext() {
        SecurityContextHolder.clearContext();
    }

    @Test
    @DisplayName("Does nothing but call filter chain if header is not present")
    void headerNotPresent() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn(null);
        filter.doFilterInternal(request, response, filterChain);
        verify(request).getHeader("Authorization");
        verify(filterChain).doFilter(request, response);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    @DisplayName("Does nothing but call filter chain if header does not match expected type")
    void headerNotMatching() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("abcd-1234");
        filter.doFilterInternal(request, response, filterChain);
        verify(request).getHeader("Authorization");
        verify(filterChain).doFilter(request, response);
        assertThat(SecurityContextHolder.getContext().getAuthentication()).isNull();
    }

    @Test
    @DisplayName("Does set authentication on security context with matching header")
    void authenticationFails() throws ServletException, IOException {
        when(request.getHeader("Authorization")).thenReturn("Bearer abcd-1234");
        filter.doFilterInternal(request, response, filterChain);
        verify(request).getHeader("Authorization");
        verify(filterChain).doFilter(request, response);
        assertThat(SecurityContextHolder.getContext().getAuthentication())
                .isInstanceOf(RawFirebaseAuthenticationToken.class)
                .extracting(Authentication::getCredentials).isEqualTo("abcd-1234");
    }
}