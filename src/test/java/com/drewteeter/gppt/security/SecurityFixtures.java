package com.drewteeter.gppt.security;

public class SecurityFixtures {
    public static final UserFixture VALID_USER_FIXTURE_1 = new UserFixture("ajiewjfio3847197413", "abcd-1234");
    public static final UserFixture VALID_USER_FIXTURE_2 = new UserFixture("fjewjio3u49817jwnef", "abcd-1236");
    public static final UserFixture INVALID_USER_FIXTURE = new UserFixture("jfiwe47jfewj2741", "abcd-1235");

    public static final class UserFixture {
        private String token;
        private String uid;

        public UserFixture(String token, String uid) {
            this.token = token;
            this.uid = uid;
        }

        public String getToken() {
            return token;
        }

        public String getUid() {
            return uid;
        }

        public String authHeader() {
            return "Bearer " + token;
        }
    }
}
