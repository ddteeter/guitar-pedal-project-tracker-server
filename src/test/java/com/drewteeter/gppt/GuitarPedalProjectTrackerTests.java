package com.drewteeter.gppt;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@EnabledIfEnvironmentVariable(matches = "^(?=\\s*\\S).*$", named = "GOOGLE_APPLICATION_CREDENTIALS")
public class GuitarPedalProjectTrackerTests {
  @Test
  public void contextLoads() {
  }
}
