package com.drewteeter.gppt;

import com.google.cloud.NoCredentials;
import com.google.cloud.datastore.DatastoreOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import com.googlecode.objectify.ObjectifyFactory;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.Slf4jLogConsumer;
import org.testcontainers.images.builder.ImageFromDockerfile;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static com.drewteeter.gppt.security.SecurityFixtures.*;
import static org.mockito.Mockito.lenient;
import static org.mockito.MockitoAnnotations.initMocks;

@ExtendWith({ SpringExtension.class, MockitoExtension.class })
@SpringBootTest
@ActiveProfiles("int-test")
@AutoConfigureMockMvc
@Testcontainers
@ContextConfiguration(initializers = BaseIT.Initializer.class)
public abstract class BaseIT {
    private static final Logger LOGGER = LoggerFactory.getLogger(BaseIT.class);

    @Container
    public static GenericContainer datastore = new GenericContainer(
            new ImageFromDockerfile("testcontainers/gppt-google-cloud-datastore", false)
                    .withFileFromClasspath("Dockerfile", "docker/google-cloud/datastore/Dockerfile")
    ).withExposedPorts(8888).withLogConsumer(new Slf4jLogConsumer(LOGGER));

    @Autowired
    protected MockMvc mockMvc;

    @MockBean protected FirebaseAuth firebaseAuth;

    @Mock
    protected FirebaseToken validUserToken1;

    @Mock
    protected FirebaseToken validUserToken2;

    @BeforeEach
    public void mockitoInit() throws FirebaseAuthException {
        initMocks(this);
        lenient().when(firebaseAuth.verifyIdToken(VALID_USER_FIXTURE_1.getToken())).thenReturn(validUserToken1);
        lenient().when(firebaseAuth.verifyIdToken(VALID_USER_FIXTURE_2.getToken())).thenReturn(validUserToken2);
        lenient().when(firebaseAuth.verifyIdToken(INVALID_USER_FIXTURE.getToken()))
                .thenThrow(new FirebaseAuthException("INVALID", "Invalid token"));

        lenient().when(validUserToken1.getUid()).thenReturn(VALID_USER_FIXTURE_1.getUid());
        lenient().when(validUserToken2.getUid()).thenReturn(VALID_USER_FIXTURE_2.getUid());
    }

    public static class Initializer implements ApplicationContextInitializer<GenericApplicationContext> {
        @Override
        public void initialize(@NotNull GenericApplicationContext genericApplicationContext) {
            genericApplicationContext.registerBean(ObjectifyFactory.class,
                    () -> new ObjectifyFactory(
                            DatastoreOptions.newBuilder()
                                    .setHost(datastore.getContainerIpAddress() + ":" + datastore.getMappedPort(8888))
                                    .setProjectId("gppt-test")
                                    .setCredentials(NoCredentials.getInstance())
                                    .build()
                                    .getService()
                    ));
        }
    }
}
