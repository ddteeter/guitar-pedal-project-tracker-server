package com.drewteeter.gppt.project;

import com.drewteeter.gppt.BaseIT;
import com.jayway.jsonpath.JsonPath;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.*;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Optional;

import static com.drewteeter.gppt.security.SecurityFixtures.*;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ProjectIT extends BaseIT {
    @Test
    @DisplayName("Fails for unauthenticated user")
    @Order(1)
    public void failsForUnauthenticatedUser() throws Exception {
        mockMvc.perform(get("/projects").header(AUTHORIZATION, INVALID_USER_FIXTURE.authHeader()))
                .andExpect(status().isForbidden());

        mockMvc.perform(get("/projects/1234").header(AUTHORIZATION, INVALID_USER_FIXTURE.authHeader()))
                .andExpect(status().isForbidden());

        mockMvc.perform(put("/projects/1234").header(AUTHORIZATION, INVALID_USER_FIXTURE.authHeader()))
                .andExpect(status().isForbidden());

        mockMvc.perform(delete("/projects").header(AUTHORIZATION, INVALID_USER_FIXTURE.authHeader()))
                .andExpect(status().isForbidden());

        mockMvc.perform(post("/projects").header(AUTHORIZATION, INVALID_USER_FIXTURE.authHeader()))
                .andExpect(status().isForbidden());
    }

    @Test
    @DisplayName("Given empty datastore, retrieving projects for fixture user 1 does return no results")
    @Order(2)
    public void emptyDataStoreGetProjects() throws Exception {
        mockMvc.perform(get("/projects").header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));
    }

    @Test
    @DisplayName("Creates projects isolated to users")
    @Order(3)
    public void projectCreationUserIsolated() throws Exception {
        Long user1ProjectId = createProject(VALID_USER_FIXTURE_1, "Test Project", Optional.empty());

        checkProjectExistsInProjects(VALID_USER_FIXTURE_1, user1ProjectId, "Test Project");

        mockMvc.perform(get("/projects").header(AUTHORIZATION, VALID_USER_FIXTURE_2.authHeader()))
                .andExpect(status().isOk())
                .andExpect(content().json("[]"));

        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "Test Project",
                Optional.of(not(equalTo(user1ProjectId))));

        assertThat(user1ProjectId).isNotEqualTo(user2ProjectId);
        checkProjectExistsInProjects(VALID_USER_FIXTURE_1, user1ProjectId, "Test Project")
                .andExpect(jsonPath("$.length()").value(1));

        checkProjectExistsInProjects(VALID_USER_FIXTURE_2, user2ProjectId, "Test Project")
                .andExpect(jsonPath("$.length()").value(1));
    }

    @Test
    @DisplayName("User is able to delete only their own projects")
    @Order(4)
    public void deleteOwnProjects() throws Exception {
        Long user1ProjectId = createProject(VALID_USER_FIXTURE_1, "To Be Deleted", Optional.empty());
        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "To Be Deleted", Optional.empty());

        MvcResult result = checkProjectExistsInProjects(VALID_USER_FIXTURE_1, user1ProjectId, "To Be Deleted")
                .andReturn();
        int user1ProjectsCount = JsonPath.parse(result.getResponse().getContentAsString()).read("$.length()");

        mockMvc.perform(delete("/projects/" + user1ProjectId)
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/projects").header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()").value(user1ProjectsCount - 1));

        checkProjectExistsInProjects(VALID_USER_FIXTURE_2, user2ProjectId, "To Be Deleted");

        mockMvc.perform(delete("/projects/" + user2ProjectId)
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isNoContent());

        checkProjectExistsInProjects(VALID_USER_FIXTURE_2, user2ProjectId, "To Be Deleted");
    }

    @Test
    @DisplayName("User is able to retrieve only their own projects")
    @Order(5)
    public void userCanRetrieveOnlyOwnProjects() throws Exception {
        Long user1ProjectId = createProject(VALID_USER_FIXTURE_1, "Project", Optional.empty());
        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "Project", Optional.empty());

        checkProjectExists(VALID_USER_FIXTURE_1, user1ProjectId, "Project");
        checkProjectExists(VALID_USER_FIXTURE_2, user2ProjectId, "Project");

        mockMvc.perform(get("/projects/" + user2ProjectId)
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("User can update only their own projects")
    @Order(6)
    public void usersCanUpdateOwnProject() throws Exception {
        Long user1ProjectId1 = createProject(VALID_USER_FIXTURE_1, "Project 1", Optional.empty());
        Long user1ProjectId2 = createProject(VALID_USER_FIXTURE_1, "Project 2", Optional.empty());
        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "Project", Optional.empty());

        mockMvc.perform(put("/projects/" + user1ProjectId1)
                        .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\":\"" + user1ProjectId1 + "\", \"name\":\"Project 1 New\"}"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(user1ProjectId1))
                .andExpect(jsonPath("$.name").value("Project 1 New"));

        checkProjectExists(VALID_USER_FIXTURE_1, user1ProjectId1, "Project 1 New");
        checkProjectExists(VALID_USER_FIXTURE_1, user1ProjectId2, "Project 2");
        checkProjectExists(VALID_USER_FIXTURE_2, user2ProjectId, "Project");
    }

    @Test
    @DisplayName("User can add build docs to only their own projects")
    @Order(7)
    public void usersCanAddBuildDocsToTheirOwnProjects() throws Exception {
        Long user1ProjectId = createProject(VALID_USER_FIXTURE_1, "Project", Optional.empty());
        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "Project", Optional.empty());

        Long buildDocId = createBuildDoc(VALID_USER_FIXTURE_1, user1ProjectId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");

        checkBuildDocExists(VALID_USER_FIXTURE_1, user1ProjectId, buildDocId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");

        mockMvc.perform(get("/projects/" + user2ProjectId + "/buildDoc")
                .header(AUTHORIZATION, VALID_USER_FIXTURE_2.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$.length()").value(0));

        mockMvc.perform(post("/projects/" + user2ProjectId + "/buildDoc")
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromUrl\":\"https://www.pedalbuildpipeline.com/test-doc.pdf\"}"))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("User can delete build docs from only their own projects")
    @Order(8)
    public void usersCanDeleteBuildDocsOnTheirOwnProjects() throws Exception {
        Long user1ProjectId = createProject(VALID_USER_FIXTURE_1, "Project", Optional.empty());
        Long user1BuildDocId = createBuildDoc(VALID_USER_FIXTURE_1, user1ProjectId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");
        Long user2ProjectId = createProject(VALID_USER_FIXTURE_2, "Project", Optional.empty());
        Long user2BuildDocId = createBuildDoc(VALID_USER_FIXTURE_2, user2ProjectId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");

        assertThat(user1BuildDocId).isNotEqualTo(user2BuildDocId);
        checkBuildDocExists(VALID_USER_FIXTURE_1, user1ProjectId, user1BuildDocId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");
        checkBuildDocExists(VALID_USER_FIXTURE_2, user2ProjectId, user2BuildDocId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");

        MvcResult user1BuildDocsResult = mockMvc.perform(get("/projects/" + user1ProjectId + "/buildDoc")
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isOk())
                .andReturn();
        int user1BuildDocsCount = JsonPath.parse(user1BuildDocsResult.getResponse().getContentAsString()).read("$.length()");

        mockMvc.perform(delete("/projects/" + user1ProjectId + "/buildDoc/" + user1BuildDocId)
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isNoContent());

        mockMvc.perform(get("/projects/" + user1ProjectId + "/buildDoc")
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()").value(user1BuildDocsCount - 1));
        checkBuildDocExists(VALID_USER_FIXTURE_2, user2ProjectId, user2BuildDocId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");

        mockMvc.perform(delete("/projects/" + user2ProjectId + "/buildDoc/" + user2BuildDocId)
                .header(AUTHORIZATION, VALID_USER_FIXTURE_1.authHeader()))
                .andExpect(status().isNoContent());

        checkBuildDocExists(VALID_USER_FIXTURE_2, user2ProjectId, user2BuildDocId,
                "https://www.pedalbuildpipeline.com/test-doc.pdf");
    }

    protected ResultActions checkProjectExistsInProjects(UserFixture user, Long projectId, String projectName) throws Exception {
        return mockMvc.perform(get("/projects").header(AUTHORIZATION, user.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[?(@.id == '" + projectId + "')].name").value(projectName));
    }

    protected void checkProjectExists(UserFixture user, Long projectId, String projectName) throws Exception {
        mockMvc.perform(get("/projects/" + projectId)
                .header(AUTHORIZATION, user.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(projectId))
                .andExpect(jsonPath("$.name").value(projectName));
    }

    protected void checkBuildDocExists(UserFixture user, Long projectId, Long buildDocId, String fromUrl) throws Exception {
        mockMvc.perform(get("/projects/" + projectId + "/buildDoc")
                .header(AUTHORIZATION, user.authHeader()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[?(@.id == '" + buildDocId + "')].fromUrl").value(fromUrl));
    }

    protected Long createProject(UserFixture user, String projectName, Optional<Matcher<?>> idMatcher) throws Exception {
        ResultActions resultActions = mockMvc.perform(
                post("/projects")
                        .header(AUTHORIZATION, user.authHeader())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\":\"" + projectName + "\"}")
        ).andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.name").value(projectName));

        if (idMatcher.isPresent()) {
            resultActions.andExpect(jsonPath("$.id").value(idMatcher.get()));
        }

        MvcResult result = resultActions.andReturn();
        return JsonPath.parse(result.getResponse().getContentAsString()).read("$.id", Long.class);
    }

    protected Long createBuildDoc(UserFixture user, Long projectId, String fromUrl) throws Exception {
        MvcResult result = mockMvc.perform(post("/projects/" + projectId + "/buildDoc")
                .header(AUTHORIZATION, user.authHeader())
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"fromUrl\":\"" + fromUrl + "\"}"))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.fromUrl").value(fromUrl))
                .andReturn();
        return JsonPath.parse(result.getResponse().getContentAsString()).read("$.id", Long.class);
    }
}