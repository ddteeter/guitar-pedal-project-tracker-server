package com.drewteeter.gppt.component.view;

public class ComponentView {
    private Long id;
    private String type;
    private String value;
    private String unit;

    public ComponentView(Long id, String type, String value, String unit) {
        this.id = id;
        this.type = type;
        this.value = value;
        this.unit = unit;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
