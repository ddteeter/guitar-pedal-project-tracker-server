package com.drewteeter.gppt.component.model;

import com.drewteeter.gppt.project.model.Project;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

public class Component {
    @Id
    private Long id;
    @Parent
    private Key<Project> project;
    private String type;
    private String value;
    private String unit;

    private Component() {}

    public Component(Project project, String type, String value, String unit) {
        this.project = Key.create(project);
        this.type = type;
        this.value = value;
        this.unit = unit;
    }

    public Component(Long id, Project project, String type, String value, String unit) {
        this(project, type, value, unit);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }
}
