package com.drewteeter.gppt.user;

import com.drewteeter.gppt.user.model.User;

import java.util.Optional;

public interface UserService {
    Optional<User> getUserById(String id);

    User saveUser(User user);
}
