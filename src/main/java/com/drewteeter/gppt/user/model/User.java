package com.drewteeter.gppt.user.model;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

import java.util.Objects;
import java.util.Set;

@Entity
public class User {
    @Id
    private String id;
    private String email;
    private String name;
    private String pictureUri;
    private Set<String> roles;

    private User() {}

    public User(String id, String email, String name, String pictureUri, Set<String> roles) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.pictureUri = pictureUri;
        this.roles = roles;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getName() {
        return name;
    }

    public String getPictureUri() {
        return pictureUri;
    }

    public Set<String> getRoles() {
        return roles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return id.equals(user.id) &&
                email.equals(user.email) &&
                Objects.equals(name, user.name) &&
                Objects.equals(pictureUri, user.pictureUri) &&
                roles.equals(user.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, name, pictureUri, roles);
    }
}
