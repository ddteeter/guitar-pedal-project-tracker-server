package com.drewteeter.gppt.user.impl;

import com.drewteeter.gppt.user.model.User;
import com.drewteeter.gppt.user.UserStore;
import com.googlecode.objectify.Key;
import org.springframework.stereotype.Repository;

import java.util.Optional;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Repository
public class ObjectifyUserStore implements UserStore {
    @Override
    public Optional<User> getUserById(String id) {
        return Optional.ofNullable(ofy().load().key(Key.create(User.class, id)).now());
    }

    @Override
    public User saveUser(User user) {
        ofy().save().entity(user).now();
        return user;
    }
}
