package com.drewteeter.gppt.user.impl;

import com.drewteeter.gppt.user.model.User;
import com.drewteeter.gppt.user.UserService;
import com.drewteeter.gppt.user.UserStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserServiceImpl implements UserService {
    private UserStore userStore;

    @Autowired
    public UserServiceImpl(UserStore userStore) {
        this.userStore = userStore;
    }

    @Override
    public Optional<User> getUserById(String id) {
        return userStore.getUserById(id);
    }

    @Override
    public User saveUser(User user) {
        return userStore.saveUser(user);
    }
}
