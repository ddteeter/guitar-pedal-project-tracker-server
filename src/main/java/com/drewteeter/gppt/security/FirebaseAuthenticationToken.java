package com.drewteeter.gppt.security;

import com.drewteeter.gppt.user.model.User;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.validation.constraints.NotNull;
import java.util.stream.Collectors;

public class FirebaseAuthenticationToken extends AbstractAuthenticationToken {
    private final User principal;
    private final FirebaseToken credentials;

    public FirebaseAuthenticationToken(@NotNull User principal, @NotNull FirebaseToken credentials) {
        super(principal.getRoles().stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList()));
        this.principal = principal;
        this.credentials = credentials;
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }
}
