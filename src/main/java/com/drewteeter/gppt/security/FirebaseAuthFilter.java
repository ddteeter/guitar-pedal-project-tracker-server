package com.drewteeter.gppt.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class FirebaseAuthFilter extends OncePerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(FirebaseAuthFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        Optional<String> firebaseTokenHeader = getFirebaseToken(httpServletRequest);

        if (firebaseTokenHeader.isPresent()) {
            RawFirebaseAuthenticationToken rawToken = new RawFirebaseAuthenticationToken(firebaseTokenHeader.get());
            rawToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            SecurityContextHolder.getContext().setAuthentication(rawToken);
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    protected Optional<String> getFirebaseToken(HttpServletRequest httpServletRequest) {
        Optional<String> firebaseToken = Optional.empty();

        String authHeader = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        if (!StringUtils.isEmpty(authHeader) && authHeader.startsWith("Bearer ")) {
            firebaseToken = Optional.of(authHeader.replace("Bearer ", ""));
        }

        return firebaseToken;
    }
}
