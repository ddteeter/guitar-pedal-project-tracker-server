package com.drewteeter.gppt.security;

import com.drewteeter.gppt.user.model.User;
import com.drewteeter.gppt.user.UserService;
import com.google.common.collect.Sets;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class FirebaseAuthProvider implements AuthenticationProvider {
    private static final Logger logger = LoggerFactory.getLogger(FirebaseAuthProvider.class);
    private FirebaseAuth firebaseAuth;
    private UserService userService;

    @Autowired
    public FirebaseAuthProvider(FirebaseAuth firebaseAuth, UserService userService) {
        this.firebaseAuth = firebaseAuth;
        this.userService = userService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        if (supports(authentication.getClass())) {
            try {
                final FirebaseToken firebaseToken = firebaseAuth.verifyIdToken((String) authentication.getCredentials());
                User user = userService.getUserById(firebaseToken.getUid()).orElseGet(() ->
                        userService.saveUser(new User(firebaseToken.getUid(), firebaseToken.getEmail(), firebaseToken.getName(),
                                firebaseToken.getPicture(), Sets.newHashSet(Roles.USER.name()))));

                return new FirebaseAuthenticationToken(user, firebaseToken);
            } catch (FirebaseAuthException e) {
                logger.error("Unable to validate Firebase token, encountered error", e);
            }
        }

        return null;
    }

    @Override
    public boolean supports(Class<?> authenticationClass) {
        return RawFirebaseAuthenticationToken.class.isAssignableFrom(authenticationClass);
    }
}
