package com.drewteeter.gppt.objectify;

import com.drewteeter.gppt.project.model.BuildDoc;
import com.drewteeter.gppt.project.model.Project;
import com.drewteeter.gppt.user.model.User;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class ObjectifyContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ObjectifyContextListener.class);

    @Autowired(required = false)
    private ObjectifyFactory objectifyFactory;

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (objectifyFactory != null) {
            ObjectifyService.init(objectifyFactory);
        } else {
            ObjectifyService.init();
        }

        ObjectifyService.register(User.class);
        ObjectifyService.register(Project.class);
        ObjectifyService.register(BuildDoc.class);
    }
}
