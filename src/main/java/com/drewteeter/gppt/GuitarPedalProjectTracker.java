package com.drewteeter.gppt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GuitarPedalProjectTracker {
  public static void main(String[] args) {
    SpringApplication.run(GuitarPedalProjectTracker.class, args);
  }
}
