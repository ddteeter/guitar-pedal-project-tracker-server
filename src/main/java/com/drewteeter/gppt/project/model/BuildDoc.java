package com.drewteeter.gppt.project.model;

import com.drewteeter.gppt.user.model.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

import java.util.Optional;

@Entity
public class BuildDoc {
    @Id
    private Long id;
    @Parent
    private Key<Project> project;
    private String fromUrl;

    private BuildDoc() {}

    public BuildDoc(Project project, Optional<String> fromUrl) {
        this.project = Key.create(project);
        this.fromUrl = fromUrl.orElse(null);
    }

    public BuildDoc(String userId, Long projectId, Optional<String> fromUrl) {
        this.project = Key.create(Key.create(User.class, userId), Project.class, projectId);
        this.fromUrl = fromUrl.orElse(null);
    }

    public BuildDoc(Long id, Project parent, Optional<String> fromUrl) {
        this(parent, fromUrl);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public Long getProjectId() {
        return project.getId();
    }

    public Optional<String> getFromUrl() {
        return Optional.ofNullable(fromUrl);
    }
}
