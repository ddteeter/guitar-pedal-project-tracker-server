package com.drewteeter.gppt.project.model;

import com.drewteeter.gppt.user.model.User;
import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Parent;

@Entity
public class Project {
    @Id
    private Long id;
    @Parent
    private Key<User> user;
    private String name;

    private Project() {}

    public Project(User user, String name) {
        this.user = Key.create(user);
        this.name = name;
    }

    public Project(Long id, User user, String name) {
        this(user, name);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
