package com.drewteeter.gppt.project;

import com.drewteeter.gppt.exception.ParentObjectNotFoundException;
import com.drewteeter.gppt.project.model.BuildDoc;
import com.drewteeter.gppt.project.model.Project;
import com.drewteeter.gppt.project.view.BuildDocView;
import com.drewteeter.gppt.project.view.ProjectView;
import com.drewteeter.gppt.user.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/projects")
public class ProjectController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);
    private ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<ProjectView> getAllUsersProjects(@AuthenticationPrincipal User user) {
        return projectService.getAllProjectsForUser(user).stream().map(this::toView).collect(Collectors.toList());
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ProjectView createProject(@AuthenticationPrincipal User user, @RequestBody ProjectView project) {
        return toView(projectService.saveProject(fromView(user, project)));
    }

    @GetMapping("/{id}")
    public ProjectView getProject(@AuthenticationPrincipal User user, @PathVariable("id") Long id) {
        return toView(projectService.getProjectById(user, id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    @PutMapping("/{id}")
    public ProjectView updateProject(@AuthenticationPrincipal User user, @PathVariable("id") Long projectId,
                                     @RequestBody ProjectView project) {
        return toView(projectService.saveProject(fromView(user, new ProjectView(projectId, project.getName()))));
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProject(@AuthenticationPrincipal User user, @PathVariable("id") Long projectId) {
        projectService.deleteProject(user, projectId);
    }

    @PostMapping("/{id}/buildDoc")
    @ResponseStatus(code = HttpStatus.CREATED)
    public BuildDocView addProjectBuildDoc(@AuthenticationPrincipal User user,
                                           @PathVariable("id") Long projectId, @RequestBody BuildDocView buildDocView) {
        try {
            return toView(projectService.addBuildDoc(user, new BuildDoc(user.getId(), projectId,
                    Optional.ofNullable(buildDocView.getFromUrl()))));
        } catch (ParentObjectNotFoundException e) {
            LOGGER.error("Unable to save build doc", e);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Desired project now found");
        }
    }

    @GetMapping("/{id}/buildDoc")
    public List<BuildDocView> getProjectBuildDocs(@AuthenticationPrincipal User user,
                                    @PathVariable("id") Long projectId) {
        return projectService.getProjectBuildDocs(user, projectId).stream()
                .map(this::toView).collect(Collectors.toList());
    }

    @DeleteMapping("/{projectId}/buildDoc/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteProjectBuildDoc(@AuthenticationPrincipal User user,
                                      @PathVariable("projectId") Long projectId,
                                      @PathVariable("id") Long id) {
        projectService.deleteBuildDoc(user, projectId, id);
    }

    protected Project fromView(User user, ProjectView project) {
        return new Project(project.getId(), user, project.getName());
    }

    protected ProjectView toView(Project project) {
        return new ProjectView(project.getId(), project.getName());
    }

    protected BuildDocView toView(BuildDoc buildDoc) {
        return new BuildDocView(buildDoc.getId(), buildDoc.getFromUrl().orElse(null));
    }
}
