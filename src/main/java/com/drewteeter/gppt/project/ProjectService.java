package com.drewteeter.gppt.project;

import com.drewteeter.gppt.exception.ParentObjectNotFoundException;
import com.drewteeter.gppt.project.model.BuildDoc;
import com.drewteeter.gppt.project.model.Project;
import com.drewteeter.gppt.user.model.User;

import java.util.List;
import java.util.Optional;

public interface ProjectService {
    List<Project> getAllProjectsForUser(User user);

    Project saveProject(Project project);

    Optional<Project> getProjectById(User user, Long id);

    void deleteProject(User user, Long projectId);

    BuildDoc addBuildDoc(User user, BuildDoc buildDoc) throws ParentObjectNotFoundException;

    void deleteBuildDoc(User user, Long projectId, Long buildDocId);

    List<BuildDoc> getProjectBuildDocs(User user, Long proejctId);
}
