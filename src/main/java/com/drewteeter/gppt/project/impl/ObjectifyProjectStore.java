package com.drewteeter.gppt.project.impl;

import com.drewteeter.gppt.project.ProjectStore;
import com.drewteeter.gppt.project.model.BuildDoc;
import com.drewteeter.gppt.project.model.Project;
import com.drewteeter.gppt.user.model.User;
import com.googlecode.objectify.Key;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.googlecode.objectify.ObjectifyService.ofy;

@Component
public class ObjectifyProjectStore implements ProjectStore {
    @Override
    public List<Project> getAllProjectsForUser(User user) {
        return ofy().load().type(Project.class).ancestor(user).list();
    }

    @Override
    public Project saveProject(Project project) {
        ofy().save().entity(project).now();
        return project;
    }

    @Override
    public Optional<Project> getProjectById(User user, Long id) {
        return Optional.ofNullable(ofy().load().key(Key.create(Key.create(user), Project.class, id)).now());
    }

    @Override
    public void deleteProject(User user, Long projectId) {
        ofy().delete().key(Key.create(Key.create(user), Project.class, projectId)).now();
    }

    @Override
    public BuildDoc addBuildDoc(BuildDoc buildDoc) {
        ofy().save().entity(buildDoc).now();
        return buildDoc;
    }

    @Override
    public void deleteBuildDoc(User user, Long projectId, Long buildDocId) {
        ofy().delete().key(Key.create(Key.create(Key.create(user), Project.class, projectId),
                BuildDoc.class, buildDocId));
    }

    @Override
    public List<BuildDoc> getProjectBuildDocs(User user, Long projectId) {
        return ofy().load().type(BuildDoc.class).ancestor(Key.create(Key.create(user), Project.class, projectId)).list();
    }
}
