package com.drewteeter.gppt.project.impl;

import com.drewteeter.gppt.exception.ParentObjectNotFoundException;
import com.drewteeter.gppt.project.ProjectService;
import com.drewteeter.gppt.project.ProjectStore;
import com.drewteeter.gppt.project.model.BuildDoc;
import com.drewteeter.gppt.project.model.Project;
import com.drewteeter.gppt.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ProjectServiceImpl implements ProjectService {
    private ProjectStore projectStore;

    @Autowired
    public ProjectServiceImpl(ProjectStore projectStore) {
        this.projectStore = projectStore;
    }

    @Override
    public List<Project> getAllProjectsForUser(User user) {
        return projectStore.getAllProjectsForUser(user);
    }

    @Override
    public Project saveProject(Project project) {
        return projectStore.saveProject(project);
    }

    @Override
    public Optional<Project> getProjectById(User user, Long id) {
        return projectStore.getProjectById(user, id);
    }

    @Override
    public void deleteProject(User user, Long projectId) {
        projectStore.deleteProject(user, projectId);
    }

    @Override
    public BuildDoc addBuildDoc(User user, BuildDoc buildDoc) throws ParentObjectNotFoundException {
        Optional<Project> project = projectStore.getProjectById(user, buildDoc.getProjectId());
        if (project.isPresent()) {
            return projectStore.addBuildDoc(buildDoc);
        } else {
            throw new ParentObjectNotFoundException("Project with ID: " + buildDoc.getProjectId() +
                    " not found for user: " + user.getId());
        }
    }

    @Override
    public void deleteBuildDoc(User user, Long projectId, Long buildDocId) {
        projectStore.deleteBuildDoc(user, projectId, buildDocId);
    }

    @Override
    public List<BuildDoc> getProjectBuildDocs(User user, Long projectId) {
        return projectStore.getProjectBuildDocs(user, projectId);
    }
}
