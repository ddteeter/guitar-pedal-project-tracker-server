package com.drewteeter.gppt.project.view;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class BuildDocView {
    private Long id;
    private String fromUrl;

    @JsonCreator
    public BuildDocView(@JsonProperty("id") Long id, @JsonProperty("fromUrl") String fromUrl) {
        this.id = id;
        this.fromUrl = fromUrl;
    }

    public Long getId() {
        return id;
    }

    public String getFromUrl() {
        return fromUrl;
    }
}
