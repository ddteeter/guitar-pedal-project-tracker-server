package com.drewteeter.gppt.exception;

public class ParentObjectNotFoundException extends Exception {
    public ParentObjectNotFoundException(String message) {
        super(message);
    }
}
